import {  Injectable} from '@angular/core';
import {  HttpClient} from '@angular/common/http';
import {  MensajeService} from './mensaje.service';
import {  FormGroup} from '@angular/forms';
import {  Observable,  of} from 'rxjs';
import {  catchError,  tap} from 'rxjs/operators';
import {  categoria} from '../Model/categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private apiUrl = 'http://localhost:8000/categorias';
  constructor(private http: HttpClient, private mensajeService: MensajeService) {}

  getCat(): Observable<categoria[]>{
    return this.http.get<categoria[]>(this.apiUrl)
      .pipe(
        tap(cat => this.log(`fetched publicaciones`)),
        catchError(this.handleError('getpublic', []))
      );
  }

  addCategoria(data: FormGroup): Observable <categoria> {
    const datos = data.value;
    return this.http.post <categoria> (this.apiUrl, datos).pipe(
      tap((c: categoria) => this.log(`categoria creada ${c.nombre}`)),
      catchError(this.handleError <categoria> ('addC'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */

  private handleError < T >(operation = 'operaciones', result ?: T) {
    return (error: any): Observable < T > => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result);
    };
  }

  private log(message: string) {
    this.mensajeService.add('categoria' + message);
  }
}
