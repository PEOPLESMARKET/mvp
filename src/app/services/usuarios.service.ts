import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { user } from '../Model/user';
import { catchError, map, tap } from 'rxjs/operators';
import {MensajeService } from './mensaje.service';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {


  private apiUrl = 'http://localhost:8000/usuarios';
  constructor(
    private http: HttpClient,
    private mensajeService: MensajeService) { }

    getUsuarios(): Observable<user[]>{
      return this.http.get<user[]>(this.apiUrl)
      .pipe(
        tap(usuarios => this.log(`fetched usuarios`)),
        catchError(this.handleError('getUsuario', []))
      );
    }

    getUsuario404<Data>(id: string): Observable<user>{
      const url = `${this.apiUrl}/?id=${id}`;
      return this.http.get<user[]>(url)
      .pipe(
        map(usuarios => usuarios[0]),
        tap(u => {
          const outcome = u ? `fetched` : `did not find`;
          this.log(`${outcome} user id=${id}`);
        }),
        catchError(this.handleError<user>(`getUsuario id=${id}`))
      );
    }

    getUsuario(id: string): Observable<user>{
      const url = `${this.apiUrl}/${id}`;
      console.log(url);
      return this.http.get<user>(url).pipe(
        tap(_ => this.log(`fetched user id=${id}`)),
        catchError(this.handleError<user>(`getUsuario id=${id}`))
      );

    }

    buscarUsuario(term: string): Observable<user>{
      if (!term.trim()){
        return of(null);
      }
      const url = `${this.apiUrl}/${term}`;
      console.log(url);

      return this.http.get<user>(url).pipe(
        tap(_ => this.log(`found usuario matching"${term}"`)),
        catchError(this.handleError<user>('buscarUsuarios'))
      );
    }

    addUsuario(usuario: FormGroup): Observable<user>{
      const datos = usuario.value;
      return this.http.post<user>(this.apiUrl, datos).pipe(
        tap((u: user) => this.log(`usuario creado w/ documento=${u.documento} `)),
        catchError(this.handleError<user>('addUsuario'))
      );
    }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
    private handleError<T>(operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {


        console.error(error);


        this.log(`${operation} failed: ${error.mensaje}`);


        return of(result);
      };
    }

    private log(mensaje: string) {
      this.mensajeService.add('usuarios: ' + mensaje);
    }
  }


