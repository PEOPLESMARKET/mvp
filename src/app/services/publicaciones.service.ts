import {  Injectable} from '@angular/core';
import {  HttpClient} from '@angular/common/http';
import {  FormGroup} from '@angular/forms';
import {  Observable,  of} from 'rxjs';
import {  publicacion} from '../Model/publicacion';
import {  catchError,  tap} from 'rxjs/operators';
import {  MensajeService} from './mensaje.service';

@Injectable({
  providedIn: 'root'
})
export class PublicacionesService {
  private apiUrl = 'http://localhost:8000/publicaciones';
  constructor(private http: HttpClient, private mensajeService: MensajeService) {}

  getPublic(): Observable<publicacion[]>{
    return this.http.get<publicacion[]>(this.apiUrl)
      .pipe(
        tap(publicaciones => this.log(`fetched publicaciones`)),
        catchError(this.handleError('getpublic', []))
      );
  }

  addPublicaciones(data: FormGroup): Observable <publicacion> {
    const datos = data.value;
    return this.http.post < publicacion > (this.apiUrl, datos).pipe(
      tap((p: publicacion) => this.log(`publicacion creada ${p.producto}`)),
      catchError(this.handleError < publicacion > ('add'))
    );
  }

  inhabilitar(data: publicacion): Observable <publicacion> {
    const datos = data;
    const url = `${this.apiUrl}/${datos.id}`;
    return this.http.put < publicacion > (url, datos).pipe(
      tap((p: publicacion) => this.log(`publicacion modificada ${p.producto}`)),
      catchError(this.handleError < publicacion > ('inha'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */

  private handleError < T >(operation = 'operaciones', result ?: T) {
    return (error: any): Observable < T > => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result);
    };
  }

  private log(message: string) {
    this.mensajeService.add('publicacion' + message);
  }
}
