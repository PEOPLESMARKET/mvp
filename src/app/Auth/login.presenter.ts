import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


export class LoginPresenter  {
  private searchTerms: Subject<string> = new Subject();
  searchTerms$: Observable<string> = this.searchTerms.pipe(
    debounceTime(300),
    distinctUntilChanged(),
  );

  search(term: string): void {
    console.log(term);
    this.searchTerms.next(term);
  }

}
