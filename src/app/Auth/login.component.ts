import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPresenter } from './login.presenter';
import { Subject } from 'rxjs';
import { user } from 'src/app/Model/user';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login-ui',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [LoginPresenter],
})
export class LoginComponent implements OnDestroy, OnInit {
  @Input() usuarios: user[];
  @Input() title: string;
  @Output() search: EventEmitter<string> = new EventEmitter();
  private destroy: Subject<void> = new Subject();
  public loginGroup: FormGroup;
  constructor(private loginBuilder: FormBuilder, private presenter: LoginPresenter) { }

  ngOnInit(): void {
    this.buildForm();
    this.presenter.searchTerms$.pipe(
      takeUntil(this.destroy),
    ).subscribe(term => this.search.emit(term));
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }


  private buildForm(){
    this.loginGroup = this.loginBuilder.group({
    usuario: ['', [Validators.required, Validators.maxLength(25)]],
    contraseña: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  searchFor(): void {

    const term = this.loginGroup.value.usuario;
    console.log(term.toString());
    this.presenter.search(term);
  }
}
