import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPresenter } from './login.presenter';

describe('LoginPresenter', () => {
  let component: LoginPresenter;
  let fixture: ComponentFixture<LoginPresenter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPresenter ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPresenter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
