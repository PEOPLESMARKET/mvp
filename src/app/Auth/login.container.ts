import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { UsuariosService} from '../services/usuarios.service';
import { user } from 'src/app/Model/user';
import { switchMap } from 'rxjs/operators';
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-login',
  templateUrl: './login.container.html',
})
export class LoginContainerComponent {

  private searchTerms: Subject<string> = new Subject();

  user$: Observable<user> = this.searchTerms.pipe(
    switchMap(term => this.usuariosService.buscarUsuario(term)),
  );

  constructor(private usuariosService: UsuariosService) { }

  buscar(term: string): void{
      console.log(term);
      this.searchTerms.next(term);
  }
}
