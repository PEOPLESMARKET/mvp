export class publicacion {
  id: number;
  producto: string;
  marca: string;
  color: string;
  celular: number;
  metodoPago: string;
  descripcion: string;
  precio: number;
  tipoProducto: string;
  imagenes: string;
  estado: string;
}
