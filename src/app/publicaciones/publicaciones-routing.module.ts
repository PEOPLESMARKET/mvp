import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicacionesContainer } from './publicaciones.container';
import {RegistrarPContainerComponent} from './registrar-p/registrar-p.container';
import {ViewsContainerComponent } from './views/views.container';
import { CrearCategoriaContainer } from './crear-categoria/crear-categoria.container';


const routes: Routes = [
  { path: '', component: PublicacionesContainer, children: [
    { path: 'view', component: ViewsContainerComponent},
    { path: 'RegistrarP', component: RegistrarPContainerComponent},
    { path: 'CrearCategoria', component: CrearCategoriaContainer} ,
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicacionesRoutingModule { }
