import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsPresenter } from './views.presenter';

describe('ViewsPresenter', () => {
  let component: ViewsPresenter;
  let fixture: ComponentFixture<ViewsPresenter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsPresenter ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsPresenter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
