import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';
import { ViewsPresenter } from './views.presenter';
import { publicacion } from 'src/app/Model/publicacion';
import { Subject } from 'rxjs';
import { ThemePalette } from '@angular/material/core';
@Component({
  selector: 'app-views-ui',
  templateUrl: './views.component.html',
  styleUrls: ['./views.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ViewsPresenter],

})
export class ViewsComponent implements OnDestroy, OnInit {
  @Input() publicaciones: publicacion[];
  @Input() title: string;
  @Output() inhabilitar: EventEmitter<publicacion> = new EventEmitter();
  displayedColumns: string[] = ['id', 'producto', 'marca', 'color', 'celular', 'metodoPago', 'descripcion', 'precio', 'tipoProducto', 'imagenes', 'opcion'];
  private destroy: Subject<void> = new Subject();
  color: ThemePalette = 'accent';
  checked = false;
  disabled = false;
  constructor(private presenter: ViewsPresenter) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  inhaPub(name: string): void {
    this.presenter.inhaPublic(name);
  }

}
