import { Subject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


export class RegistrarPPresenter  {

 private add: Subject<FormGroup> = new Subject();
 add$: Observable<FormGroup> = this.add.pipe(
   debounceTime(300),
   distinctUntilChanged(),
 );

 adicionar(publicaion: FormGroup): void{
   console.log(publicaion);
   this.add.next(publicaion);
 }


}
