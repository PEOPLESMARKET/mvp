import { EventEmitter, Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegistrarPPresenter } from './registrar-p.presenter';
import { takeUntil } from 'rxjs/operators';
import { publicacion } from 'src/app/Model/publicacion';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './modal/modal.component';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-registrar-p-ui',
  templateUrl: './registrar-p.component.html',
  styleUrls: ['./registrar-p.component.css'],
  providers: [RegistrarPPresenter]
})
export class RegistrarPComponent implements OnDestroy, OnInit {
@Input() prueba: string;
@Input() confirmacion: publicacion;
@Output() registrar: EventEmitter <FormGroup> = new EventEmitter();

private destroy: Subject <void> = new Subject();

public registrarGroup: FormGroup;
  constructor(private registerBuilder: FormBuilder, private presenter: RegistrarPPresenter, public dialog: MatDialog) { }

  ngOnInit(): void {

    this.buildForm();
    this.presenter.add$.pipe(
      takeUntil(this.destroy),
    ).subscribe(req => this.registrar.emit(req));

  }

  ngOnDestroy(): void{
    this.destroy.next();
    this.destroy.complete();
  }

  addFor(): void{
    const datos = this.registrarGroup;
    console.log(datos.value);
    this.presenter.adicionar(datos);
  }

  private buildForm(){
    this.registrarGroup = this.registerBuilder.group({
      producto: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(35) ]],
      marca: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30) ]],
      color: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(10) ]],
      celular: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      metodoPago: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(35) ]],
      descripcion: ['', [Validators.required, Validators.minLength(15), Validators.maxLength(100) ]],
      precio: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(35)]],
      tipoProducto: ['', [Validators.required]],
      imagenes: ['', [Validators.required, Validators.minLength(4)]],
      diferente: [null]
    });
  }

  public getError(controlName: string): boolean {
    let error = false;
    const control = this.registrarGroup.get(controlName);
    if (control.touched && control.errors != null) {
      error = true;
    }
    return error;
  }

  openDialog() {
    if (this.confirmacion === null){
      this.dialog.open(ModalComponent, {
        data: {
          mensaje: 'error'
        }
      });
    }else{
      this.dialog.open(ModalComponent, {
        data: {
          mensaje: 'exito'
        }
      });
      this.confirmacion = null ;
    }
  }

  fileChange(element) {
    console.log(element.target.files);
}
}
