import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPComponent } from './registrar-p.component';

describe('RegistrarPComponent', () => {
  let component: RegistrarPComponent;
  let fixture: ComponentFixture<RegistrarPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
