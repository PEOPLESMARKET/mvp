import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  mensaje: 'exito' | 'error';
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',

})
export class ModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { }



}
