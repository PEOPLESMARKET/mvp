import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarPContainerComponent } from './registrar-p.container';

describe('RegistrarPContainer', () => {
  let component: RegistrarPContainerComponent;
  let fixture: ComponentFixture<RegistrarPContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarPContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarPContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
