import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { publicacion } from 'src/app/Model/publicacion';
import { switchMap } from 'rxjs/operators';
import { PublicacionesService } from 'src/app/services/publicaciones.service';
import { FormGroup } from '@angular/forms';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-registrar-p',
  templateUrl: './registrar-p.container.html',
})

export class RegistrarPContainerComponent  {

  private publicacionAdd: Subject<FormGroup> = new Subject();

  publicaciones$: Observable <publicacion> = this.publicacionAdd.pipe(
  switchMap(req => this.publicacionService.addPublicaciones(req)),
  );
  constructor(private publicacionService: PublicacionesService) { }


  add(req: FormGroup): void{
    console.log(req);
    this.publicacionAdd.next(req);
  }

}
