import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { categoria } from 'src/app/Model/categoria';
import {CategoriaService} from 'src/app/services/categoria.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-crear-categoria',
  templateUrl: './crear-categoria.container.html',
})
export class CrearCategoriaContainer {

  private categoriaAdd: Subject<FormGroup> = new Subject();

  categorias$: Observable<categoria> = this.categoriaAdd.pipe(
    switchMap(req => this.categoriaService.addCategoria(req)),
  );

  constructor(private categoriaService: CategoriaService) { }

  addC(req: FormGroup): void{
    console.log(req);
    this.categoriaAdd.next(req);
  }

}
