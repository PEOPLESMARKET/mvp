import {EventEmitter, Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CrearCategoriaPresenter } from './crear-categoria.presenter';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { categoria } from 'src/app/Model/categoria';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-crear-categoria-ui',
  templateUrl: './crear-categoria.component.html',
  styleUrls: ['./crear-categoria.component.css'],
  providers: [CrearCategoriaPresenter]
})
export class CrearCategoriaComponent implements OnDestroy, OnInit {
@Input() titulo: string;
@Input() confirmacion: categoria;
@Output() crear: EventEmitter <FormGroup> = new EventEmitter();

private destroy: Subject <void> = new Subject();

public crearGroup: FormGroup;

  constructor(private crearBuilder: FormBuilder, private presenter: CrearCategoriaPresenter) {}

  ngOnInit(): void {

    this.builForm();
    this.presenter.addC$.pipe(
      takeUntil(this.destroy),
    ).subscribe(req => this.crear.emit(req));
  }

  ngOnDestroy(): void{
    this.destroy.next();
    this.destroy.complete();
  }

  addFor(): void{
    const datos = this.crearGroup;
    console.log(datos.value);
    this.presenter.crear(datos);
  }

  private builForm(){
    this.crearGroup = this.crearBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
      descripcion: ['', [Validators.required, Validators.maxLength(1000)]]
    });
  }

  public getError(controlName: string): boolean {
    let error = false;
    const control = this.crearGroup.get(controlName);
    if (control.touched && control.errors != null) {
      error = true;
    }
    return error;
  }

}
